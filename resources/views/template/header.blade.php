<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="/fontawesome/js/all.js"></script>
</head>

<nav  class="navbar navbar-expand-lg navbar-light " style="background-color: #e0e0e0;">
	<a class="navbar-brand" href="#">
		<img src="/imagenes/fastfood_logo.png" width="30" height="30" alt="">
	</a>
	<a  class="navbar-brand text-right" href="#"><i class="fas fa-plus-square"></i> Nueva Orden</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul  class="navbar-nav mr-auto ">
			<li class="nav-item active">
				<a class="nav-link text-right" href="/Controllers/HistorialOrdenes"><i class="fas fa-list"></i> Órdenes<span class="sr-only">(current)</span></a>
			</li>
			
		</ul>
		
	</div>
</nav>

<div style="padding-bottom: 40px;"></div>

