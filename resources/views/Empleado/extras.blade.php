<body>


	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<center><a href="/empleado" class="btn btn-success btn-lg">Comidas</a></center>

				<div class="card" style="width: 15rem; top:30px; margin: auto;">
					<img class="card-img-top" src="/imagenes/papas.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Papas fritas</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>

				<div class="card" style="width: 15rem; top:40px;margin: auto;">
					<img class="card-img-top" src="/imagenes/pan-deajo.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Pan con ajo</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<center><a href="/bebidas" class="btn btn-success btn-lg">Bebidas</a></center>

				<div class="card" style="width: 15rem; top:30px; margin: auto;">
					<img class="card-img-top" src="/imagenes/francesa.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Papas francesas</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>

				<div class="card" style="width: 15rem; top:40px; margin: auto;">
					<img class="card-img-top" src="/imagenes/palitro.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Palitroques</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<center><a href="/extras" class="btn btn-success btn-lg">Extras</a></center>

				<div class="card" style="width: 15rem; top:30px; margin: auto;">
					<img class="card-img-top" src="/imagenes/aros-de-cebolla.png" style="height: 13rem;"alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Aros de cebolla</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>

				<div class="card" style="width: 15rem; top:40px; margin: auto;">
					<img class="card-img-top" src="/imagenes/cond.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Papas barbecue</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>
			</div>


			<div class="col-md-3">
				<div class="card" style="width: 20rem;background-color: #e0e0e0; top:10px; margin: auto; box-shadow: 0 2px 4px #000">
					<div class="card-body" >
						<h3>Nueva Orden</h3>
						<input type="text" name="" placeholder="Nombre del cliente"><br>

						<h3>Orden</h3><br><br>

						<input type="text" name=""><br>
						<input type="text" name=""><br><br>

						<div style="width: 17rem; height: 6rem; background-color: white;"><h4>Total:</h2></div><br>

							<button type="button" class="btn btn-success btn-lg btn-block">Enviar a cocina</button>
							<button type="button" class="btn btn-primary btn-lg btn-block">Generar ticket <i class="fas fa-share-square"></i></button>
						</div>
				</div>
			</div>                              
			</div>
		</div>
	</div>


