<body>


	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<center><a href="/empleado" class="btn btn-success btn-lg">Comidas</a></center>

				<div class="card" style="width: 15rem; top:30px; margin: auto;">
					<img class="card-img-top" src="/imagenes/coca.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Coca-cola</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>

				<div class="card" style="width: 15rem; top:40px;margin: auto;">
					<img class="card-img-top" src="/imagenes/unnamed.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">7Up</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<center><a href="/bebidas" class="btn btn-success btn-lg">Bebidas</a></center>

				<div class="card" style="width: 15rem; top:30px; margin: auto;">
					<img class="card-img-top" src="/imagenes/redbulllata.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Red-bull</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>

				<div class="card" style="width: 15rem; top:40px; margin: auto;">
					<img class="card-img-top" src="/imagenes/pepsi.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Pepsi</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<center><a href="/extras" class="btn btn-success btn-lg">Extras</a></center>

				<div class="card" style="width: 15rem; top:30px; margin: auto;">
					<img class="card-img-top" src="/imagenes/32e401_b81a344aa83c464f840236a9d3e97552_mv2.png" style="height: 13rem;"alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Té Limón</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>

				<div class="card" style="width: 15rem; top:40px; margin: auto;">
					<img class="card-img-top" src="/imagenes/fanta.png" style="height: 13rem;" alt="Card image cap">
					<div class="card-body" style="background-color: #bdbdbd;">
						<center><h5 class="card-title">Fanta</h5>
						<a href="#" class="btn btn-danger">$2.00</a>
						<a href="#" class="btn btn-warning"><i class="fas fa-plus-square"></i></a></center>
					</div>
				</div>
			</div>


			<div class="col-md-3">
				<div class="card" style="width: 20rem;background-color: #e0e0e0; top:10px; margin: auto; box-shadow: 0 2px 4px #000">
					<div class="card-body" >
						<h3>Nueva Orden</h3>
						<input type="text" name="" placeholder="Nombre del cliente"><br>

						<h3>Orden</h3><br><br>

						<input type="text" name=""><br>
						<input type="text" name=""><br><br>

						<div style="width: 17rem; height: 6rem; background-color: white;"><h4>Total:</h2></div><br>

							<button type="button" class="btn btn-success btn-lg btn-block">Enviar a cocina</button>
							<button type="button" class="btn btn-primary btn-lg btn-block">Generar ticket <i class="fas fa-share-square"></i></button>
						</div>
				</div>
			</div>
			</div>
		</div>
	</div>


