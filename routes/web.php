<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/empleado', function () {
    return view('WelcomeEmpleado');
});*/

Route::get('/empleado','EmpleadoController@index');
Route::get('/bebidas', 'EmpleadoController@bebidas');
Route::get('/extras', 'EmpleadoController@extra');

/*Route::get('/bebidas', function () {
    return view('/Empleado/bebidas');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
