<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class EmpleadoController extends Controller
{
	public function index(){
		
		$views= view('template/header').view('Empleado/WelcomeEmpleado').view('template/footer');
		return $views;
	}

	public function bebidas(){
		echo view('template/header');
		return view('Empleado/bebidas');
		echo view('template/footer');
	}

	public function extra(){
		echo view('template/header');
		return view('Empleado/extras');
		echo view('template/footer');
	}

	
}
