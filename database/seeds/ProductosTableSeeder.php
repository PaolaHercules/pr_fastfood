<?php

use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
    		'producto' => 'Hot-Dog',
    		'id_categoria' => '1',
    		'precio' => '2.00',
    	]);
    }
}
