<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('UsersTableSeeder');
        $this->call('CategoriasTableSeeder');
        $this->call('SexoTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('ProductosTableSeeder');
        $this->call('ImagenTableSeeder');
    }

}