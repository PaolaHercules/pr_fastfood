<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('categorias')->insert([
    		'nombre_categoria' => 'Comidas',
    	]);

    	DB::table('categorias')->insert([
    		'nombre_categoria' => 'Bebidas',
    	]);

    	DB::table('categorias')->insert([
    		'nombre_categoria' => 'Extras',
    	]);

    }
}
