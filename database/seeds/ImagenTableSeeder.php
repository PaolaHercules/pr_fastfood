<?php

use Illuminate\Database\Seeder;

class ImagenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imagen')->insert([
    		'id_producto' => '1',
    		'imagen' => '/imagenes/food/hot_dog.png',
    	]);
    }
}
